import { User } from '@/app/types'
import { useEffect, useReducer } from 'react'
import useLoading from './useLoading';

const ACTION_TYPES = {
    SET_USERS: "SET_USERS",
    SET_FILTER: "SET_FILTER"
}

type Action = {
    type: string;
    payload: any;
}

type State = {
    users: User[];
    filter: string;
    filteredUsers: User[];
}

const initialState = {
    users: [],
    filter: "",
    filteredUsers: []
}

const reducer = (state: State, action: Action) => {
    switch (action.type) {
        case ACTION_TYPES.SET_USERS:
            return {
                ...state,
                users: action.payload,
                filteredUsers: action.payload
            }
        case ACTION_TYPES.SET_FILTER:
            return {
                ...state,
                filter: action.payload,
                filteredUsers: state.users.filter((user)=>
                    user.firstName.toLowerCase().includes(action.payload.toLowerCase())
            ),
            };
        default:
            return state;
    }
}

const useUsers = () => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const {loading,setLoading} = useLoading();

    function fetchUsers() {
        setLoading(true);
        fetch('https://dummyjson.com/users')
          .then((data) => data.json())
          .then((data) =>{
            const payload: User[] = data.users;

            dispatch({
                type:"SET_USERS",
                payload
            });

            setLoading(false);
        })
          .catch((error) => {
            console.log("error na resposta:" + error)
            setLoading(false);
          })
    }

    useEffect(() => {
        fetchUsers();
    }, []);
        
    const setFilter = (filter: string) => {
          dispatch({
              type: ACTION_TYPES.SET_FILTER,
              payload: filter
          });   
    }

    return {
          users:state.filteredUsers,
          filter:state.filter,
          setFilter,
          loading,
          setLoading
    };
}

export default useUsers;