import {useReducer} from "react";

const ACTION_TYPES = {
  SET_LOADING: "setLoading",
};

type Action = {
  type: string;
  payload: boolean;
};

type State = {
  loading: boolean;
};

const initialState = {
  loading: true,
};

const reducer = (state:State, action:Action) => {
  switch (action.type) {
    case ACTION_TYPES.SET_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    default:
      return state;
  }
};

const useLoading = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const setLoading = (loading: boolean) => {
    dispatch({
        type: ACTION_TYPES.SET_LOADING,
        payload: loading
    });   
  }
  
  return {
    loading:state.loading,
    setLoading,
  };
};

export default useLoading;