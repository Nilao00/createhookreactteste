'use client'

import useUsers from "@/hooks/useUsers";
import { User } from '@/app/types'

export default function VersionRefactor() {

  const {
    users,
    filter,
    setFilter,
    loading
    } = useUsers();


  return (
    <div>
      <h1>Usuários listados</h1>
      <input
        placeholder="Digite um nome"
        value={filter}
        onChange={(e) => setFilter(e.target.value)}
      />
      {
        users?.length ?
      <ul>
        {
          users.map((user: User) => {
            return (
              
              <li key={user.id}>
                <img src={user.image} alt={`${user.firstName}:${user.lastName}`} width={50} height={50} />
                <span> {user.firstName}</span>
              </li>
            )
          })
        }
      </ul>:<ul><li>{loading ? 'Carregando...' : 'Nao foi encontrado registros'}</li></ul>
}
    </div>
  );
}
