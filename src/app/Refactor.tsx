import React, { useState, useEffect} from 'react';
import { User } from './types';
// import { Container } from './styles';


const Refactor: React.FC = () => {
  const [users, setUsers] = useState([]);
  const [filter, setFilter] = useState("");
  
 async function getUses() {
  await fetch('https://dummyjson.com/users')
      .then((data) => data.json())
      .then((data) =>{
        setUsers(data.users);
      })
      .catch((error) => {
        console.log("error na resposta:" + error)
      })
   }

   const usersFilter = users.filter((user: User)=>
    user.firstName.toLowerCase().includes(filter.toLowerCase()))

   useEffect(() => {
    getUses();
   }, [filter]);

  return (
    <div>
      <h1>Usuários listados</h1>
    <input
      placeholder="Digite um nome"
      value={filter}
      onChange={(e) => setFilter(e.target.value)}
    />
    {
      usersFilter?.length ?
    <ul>
      {
        usersFilter.map((user: User) => {
          return (
            <li key={user.id}>
              <img src={user.image} alt={`${user.firstName}:${user.lastName}`} width={50} height={50} />
              <span> {user.firstName}</span>
            </li>
          )
        })
      }
    </ul>
    :
    <ul>
    {
    users.map((user: User) => {
        return (          
          <li key={user.id}>
            <img src={user.image} alt={`${user.firstName}:${user.lastName}`} width={50} height={50} />
            <span> {user.firstName}</span>
          </li>
        )
      })
    }
    </ul>
}
  </div>
);
  
}

export default Refactor;